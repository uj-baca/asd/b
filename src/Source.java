import java.math.BigInteger;
import java.util.Objects;
import java.util.Scanner;

public class Source {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int z = scanner.nextInt();
		for (int i = 0; i < z; i++) {
			BinarySearchTree tree = new BinarySearchTree();
			String input = scanner.next();
			StringBuilder output = new StringBuilder();
			for (char c : input.toCharArray()) {
				if (c >= 'A' && c <= 'Z' && !output.toString().contains("" + c)) {
					tree.insert(c);
					output.append(c);
				}
			}
			System.out.print(tree.getDepth() + "," + tree.getLeavesCount() + "," + tree.countPermutations() + "," + tree.getPreOrderLeft() + "," + tree.getPreOrderRight());
			if (i != z - 1) {
				System.out.println();
			}
		}
		scanner.close();
	}
}

class BinarySearchTree {
	private Node root;
	private int leavesCount;
	private int depth;
	private String preOrderLeft = "";
	private String preOrderRight = "";

	private class Node {
		private final char data;
		private Node left;
		private Node right;
		private int height;
		private int size = 1;

		private Node(char data) {
			this.data = data;
		}
	}

	int getDepth() {
		return depth;
	}

	int getLeavesCount() {
		return leavesCount;
	}

	String getPreOrderLeft() {
		if (Objects.equals(preOrderLeft, "")) {
			return preOrderLeft(root);
		}
		return preOrderLeft;
	}

	private String preOrderLeft(Node node) {
		if (node == null) {
			return "";
		}
		preOrderLeft += node.data;
		preOrderLeft(node.left);
		preOrderLeft(node.right);
		return preOrderLeft;
	}

	String getPreOrderRight() {
		if (Objects.equals(preOrderRight, "")) {
			return preOrderRight(root);
		}
		return preOrderRight;
	}

	private String preOrderRight(Node node) {
		if (node == null) {
			return "";
		}
		preOrderRight += node.data;
		preOrderRight(node.right);
		preOrderRight(node.left);
		return preOrderRight;
	}

	void insert(char data) {
		Node newNode = new Node(data);
		if (root == null) {
			root = newNode;
			root.height = 0;
			leavesCount = 1;
			return;
		}
		Node current = root;
		Node parent;
		while (true) {
			parent = current;
			current.size++;
			if (current.data > data) {
				current = current.left;
				if (current == null) {
					parent.left = newNode;
					newNode.height = parent.height + 1;
					if (depth < newNode.height) {
						depth = newNode.height;
					}
					if (parent.right != null) {
						leavesCount++;
					}
					return;
				}
			} else {
				current = current.right;
				if (current == null) {
					parent.right = newNode;
					newNode.height = parent.height + 1;
					if (depth < newNode.height) {
						depth = newNode.height;
					}
					if (parent.left != null) {
						leavesCount++;
					}
					return;
				}
			}
		}
	}

	private BigInteger factorial(int n) {
		BigInteger fact = BigInteger.ONE;
		for (int i = 2; i <= n; i++) {
			fact = fact.multiply(new BigInteger(String.valueOf(i)));
		}
		return fact;
	}

	private BigInteger newtonSymbol(int a, int b) {
		return factorial(a).divide(factorial(b).multiply(factorial(a - b)));
	}

	BigInteger countPermutations() {
		return countPermutations(root);
	}

	private BigInteger countPermutations(Node node) {
		if (node.left == null && node.right == null) {
			return BigInteger.ONE;
		}
		if (node.left == null) {
			return countPermutations(node.right);
		}
		if (node.right == null) {
			return countPermutations(node.left);
		}
		return countPermutations(node.left).multiply(countPermutations(node.right)).multiply(newtonSymbol(node.left.size + node.right.size, node.right.size));
	}
}
